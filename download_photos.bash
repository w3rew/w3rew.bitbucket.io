input="links.txt"
counter=0
while read -r line
do
    echo $counter
    id=$(echo $line | cut -f1 -d\ )
    link=$(echo $line | cut -f2 -d\ )
    curl $link --output photos/$id.png
    counter=$((counter + 1))
done <"$input"
