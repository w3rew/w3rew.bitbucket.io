import sys
import pickle
import re

html_start = '''<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

.row {
  display: flex;
}

/* Create three equal columns that sits next to each other */
.column {
  flex: 33.33%;
  padding: 5px;
  margin-right: 0px;
  margin-left: 0px;
}
.div {
column-gap: 0px;
padding-left: 0px;
padding-right: 0px;
}
figure {
font-size: 20px;
font-style: normal;
font-weight: 400 !important
padding: 0px;
margin: 0px;
}
label{
font-weight: 400 !important}
</style>
<link rel="icon" type = "image/png" href="photos/leva.png">
 <meta property="og:image" content="./photos/leva.jpg" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="400" />
<title> Статистика Фопфа </title>
</head>
<body>
Данные с августа 2018 года по вечер 5 июня 2020 года. msg &mdash; количество любых сообщений, symb &mdash; суммарная их длина в символах,
stickers &mdash; суммарное количество отправленных стикеров. Данные могут быть не точны, особенно касающиеся стикеров, так как VK просто
так не дает доступ к API сообщений, и пришлось парсить сам сайт.
'''
html_end = '''</body>
</html>
'''



def make_entry(vk_id):
    #with open("photos/{}.png.txt".format(vk_id), 'r') as f:
    #    base = f.read()
    s =  """<div class="column">
     <figure>
    <img src="photos/{}.png" alt="{}" style="width:100%">
     <figcaption> <a href = "https://vk.com/id{}"> {}</a><br> msg: {} <br> symb: {}<br> stickers: {}</figcaption>
    </figure>
  </div>""" .format(vk_id, names[vk_id], vk_id, names[vk_id], data[vk_id][0], data[vk_id][1], data[vk_id][2])
    return s

def make_row(entries):
    res = ""
    res += '<div class="row">'
    for i in entries:
        res += make_entry(i)
    res += '</div>'
    return res


if (len(sys.argv) < 2):
    fn = 'collected_data.pickle'
else:
    fn = sys.argv[1]
with open(fn, 'rb') as f:
    data = pickle.load(f)
with open('names.pickle', 'rb') as f:
    names = pickle.load(f)


html = html_start

#messages
html += """<h1>
Пятеро с наибольшим количеством сообщений
</h1>
"""
html += make_row(sorted(data.keys(), key=lambda m: data[m][0], reverse=True)[:5])
html += """<h1>
Пятеро с наибольшей длиной сообщений
</h1>
"""
html += make_row(sorted(data.keys(), key=lambda m: data[m][1], reverse=True)[:5])
html += """
<h1>
Пятеро с наибольшим числом стикеров
</h1>
"""
html += make_row(sorted(data.keys(), key=lambda m: data[m][2], reverse=True)[:5])
html += """
<h1>
Общая статистика
</h1>
"""
buf = []
for i in data:
    buf.append(i)
    if len(buf) == 5:
        html += make_row(buf)
        buf = []
html += make_row(buf)
html += html_end
with open('index.html', 'w') as f:
    print(html, file = f)
