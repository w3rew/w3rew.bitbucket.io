import os
import sys
import codecs
from html.parser import HTMLParser
import pickle
import re
class CollectData(HTMLParser):
    def __init__(self, myid):
        self.collected_data = {}
        self.self_init()
        self.myid = myid
        super().__init__()
    def handle_starttag(self, tag, attrs):
        if (tag == 'div'):
            if (('class', 'message') in attrs):
                self.inside_message = True
                #print("Message")
            elif (len(attrs) == 0 ) and (self.inside_message):
                self.inside_text = True
                #print('Entering text')

            #and len(attrs) == 2 and (attrs[0][0] == 'class') and (attrs[0][1] == 'message'):
        elif (tag == 'a') and attrs[0][0]=='href' and (self.inside_message):
            self.link_flag = True
            self.aut_flag = True
            #print('Attrs', attrs[0][1])
            self.author = attrs[0][1]

    def handle_endtag(self, tag):
        if(tag == 'div'):
            if (self.link_flag):
                self.link_flag = False
            elif (self.inside_message):
                if not self.aut_flag:
                    self.link_flag = False
                    self.author = self.myid
                    self.aut_flag = True
                else:
                    self.inside_message = False
                    self.flush()
    def handle_data(self, data):
        if (data == 'Стикер'):
            self.has_sticker = True
        if(self.inside_text):
            #print('Text:', data)
            self.msg_txt = data
            self.inside_text = False

    def self_init(self):
        self.inside_message = False
        self.inside_text = False
        self.link_flag = False
        self.aut_flag = False
        self.msg_txt = ''
        self.author = ''
        self.has_sticker = False

    def flush(self):
        aut = re.sub('\D', '', self.author)
        if (aut == ''):
            print('tst')
        if (aut == ''):
            self.self_init()
            return
        if aut not in self.collected_data:
            self.collected_data[aut] = [1, len(self.msg_txt), int(self.has_sticker)]
        else:
            self.collected_data[aut][0] += 1
            self.collected_data[aut][1] += len(self.msg_txt)
            self.collected_data[aut][2] += self.has_sticker
        #print(self.collected_data)
        self.self_init()

parser = CollectData('290169988')
for i in os.scandir():
    if i.path.endswith(".py"): continue
    with codecs.open(i.path, 'r', 'cp1251') as f:
        data = f.read()
    parser.feed(data)
    #print(i.path)


if (len(sys.argv) > 1):
    fn = sys.argv[1]
else:
    fn = "collected_data.pickle"
with open(fn, 'wb') as f:
    pickle.dump(parser.collected_data, f)

print('Successfully collected! Active users %i' % len(parser.collected_data))
