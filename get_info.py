import sys
import pickle
from time import sleep
import requests
import re
from functools import reduce
token = '42ca017f42ca017f42ca017f5642a2fd6f442ca42ca017f1ebdf78ce91632d2da83c07c'
version = '5.92'
if (len(sys.argv) < 2):
    fn = 'collected_data.pickle'
else:
    fn = sys.argv[1]
with open(fn, 'rb') as f:
    data = pickle.load(f)
string = reduce(lambda res, x: res +  x + ',', data, '')[:-1]
resp = requests.get('https://api.vk.com/method/users.get',
            params={'user_ids':string,
                'fields':'photo_max',
                'access_token': token,
                'v':version,
                'lang':0
               }
           ).json()['response']
a = []
names = {}
for i in resp:
    a.append((i['id'], i['photo_max']))
    names[str(i['id'])] = i['first_name'] + ' ' + i['last_name']
if (len(sys.argv) < 3):
    fn = 'links.txt'
else:
    fn = sys.argv[2]
with open(fn, 'w') as f:
    for i in a:
        print(*i, sep = ' ', file = f)
with open('names.pickle', 'wb') as f:
    pickle.dump(names, f)
#    comments += comm['response']['items']
